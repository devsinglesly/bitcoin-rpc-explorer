FROM node:16.13-alpine3.14

WORKDIR /app

RUN npm install -g btc-rpc-explorer
COPY .env .
CMD btc-rpc-explorer
